webpackJsonp([42], {
    "/d1k": function(e, t, a) {
        "use strict";
        var r = c(a("GiK3")),
            l = c(a("O27J")),
            n = a("LbfT"),
            u = c(a("Do/5")),
            o = c(a("VODW")),
            s = a("WXYU");

        function c(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }
        var i = document.getElementById("react-team-page"),
            d = JSON.parse(i.dataset.team).map(function(e) {
                return {
                    name: e.name,
                    backed: e.backed,
                    groups: e.groups,
                    favoriteCategory: e.favorite_category,
                    avatarUrl: e.avatar_url,
                    hoverColor: (0, s.chooseColorBasedOnCategory)(e.favorite_category)
                }
            }),
            f = (0, n.createEnhancedStore)(o.default, {
                users: d,
                filters: {
                    displayCategory: "Everyone"
                }
            }),
            m = function() {
                var e = f.getState(),
                    t = f.dispatch;
                l.default.render(r.default.createElement(u.default, {
                    state: e,
                    dispatch: t
                }), i)
            };
        f.subscribe(m), m()
    },
    "0jCS": function(e, t, a) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        t.setDisplayCategory = function() {
            return {
                type: "SET_DISPLAY_CATEGORY",
                displayCategory: arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "Everyone"
            }
        }
    },
    De3u: function(e, t, a) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var r = function(e) {
                return e && e.__esModule ? e : {
                    default: e
                }
            }(a("GiK3")),
            l = a("Z5jf");
        t.default = function(e) {
            var t = e.state,
                a = e.onClick,
                n = e.value,
                u = n.toLowerCase().replace(/\s/g, "_");
            return r.default.createElement("button", {
                className: "type-16 grey-100 hover-highlight-soft-black keyboard-focusable pointer",
                onClick: a
            }, t.filters.displayCategory === n ? r.default.createElement("span", {
                className: "pb1-md border-bottom border-thick border-teal-500"
            }, r.default.createElement(l.BoundTranslate, {
                i18nKey: "views.site.team." + u
            })) : r.default.createElement(l.BoundTranslate, {
                i18nKey: "views.site.team." + u
            }))
        }
    },
    "Do/5": function(e, t, a) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var r = u(a("GiK3")),
            l = u(a("dOCn")),
            n = u(a("jGUh"));

        function u(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }
        t.default = function(e) {
            var t = e.state,
                a = e.dispatch;
            return r.default.createElement("div", {
                className: "border-box container-flex center mt-12 mb6 relative z99"
            }, r.default.createElement(l.default, {
                state: t,
                dispatch: a
            }), r.default.createElement(n.default, {
                state: t,
                dispatch: a
            }))
        }
    },
    IMut: function(e, t, a) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }), t.default = function(e, t) {
            var a = t.displayCategory;
            return e.filter(function(e) {
                switch (a) {
                    case "Everyone":
                        return !0;
                    default:
                        return !!e.groups && e.groups.includes(a)
                }
            }).sort(function(e, t) {
                return e.name < t.name ? -1 : 1
            })
        }
    },
    VODW: function(e, t, a) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var r = Object.assign || function(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var a = arguments[t];
                    for (var r in a) Object.prototype.hasOwnProperty.call(a, r) && (e[r] = a[r])
                }
                return e
            },
            l = {
                users: [],
                filters: {
                    displayCategory: "Everyone"
                }
            };
        t.default = function() {
            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : l,
                t = arguments[1];
            switch (t.type) {
                case "SET_DISPLAY_CATEGORY":
                    return r({}, e, {
                        filters: {
                            displayCategory: t.displayCategory
                        }
                    });
                default:
                    return e
            }
        }
    },
    Vlye: function(e, t, a) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var r = function(e) {
                return e && e.__esModule ? e : {
                    default: e
                }
            }(a("GiK3")),
            l = a("Z5jf");
        t.default = function(e) {
            var t = e.state;
            return r.default.createElement("div", null, r.default.createElement("h3", {
                className: "type-28 normal"
            }, r.default.createElement(l.BoundTranslate, {
                i18nKey: "views.site.team." + t.filters.displayCategory.toLowerCase().replace(/\s/g, "_")
            })))
        }
    },
    WXYU: function(e, t, a) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var r = t.hoverColorCodes = {
            dark: ["cobalt-500", "blue-500", "orange-500", "ksr-green-700"],
            light: ["red-400", "teal-500", "apricot-600", "grey-400"]
        };
        t.chooseColorBasedOnCategory = function(e) {
            switch (e) {
                case "Art":
                    return r.light[0];
                case "Comics":
                    return r.light[1];
                case "Crafts":
                    return r.light[2];
                case "Dance":
                    return r.light[3];
                case "Design":
                    return r.light[0];
                case "Fashion":
                    return r.light[1];
                case "Film & Video":
                    return r.light[2];
                case "Food":
                    return r.light[3];
                case "Games":
                    return r.dark[0];
                case "Journalism":
                    return r.dark[1];
                case "Music":
                    return r.dark[2];
                case "Photography":
                    return r.dark[0];
                case "Publishing":
                    return r.dark[1];
                case "Technology":
                    return r.dark[2];
                case "Theater":
                    return r.dark[0];
                default:
                    return r.dark[3]
            }
        }
    },
    dOCn: function(e, t, a) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var r = u(a("GiK3")),
            l = a("0jCS"),
            n = u(a("De3u"));

        function u(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }
        var o = ["Everyone", "Developers", "Designers", "Content", "SEO"];
        t.default = function(e) {
            var t = e.state,
                a = e.dispatch;
            return r.default.createElement("div", {
                className: "border-box bg-cobalt-600 pt16 pb16 wrap"
            }, r.default.createElement("ul", {
                className: "border-box ml5 mr5 ml12-md mr12-md"
            }, o.map(function(e) {
                return r.default.createElement("li", {
                    className: "inline-block pl2 pr2 pt1 pb1 our-team",
                    key: e
                }, r.default.createElement(n.default, {
                    state: t,
                    value: e,
                    onClick: function() {
                        return a((0, l.setDisplayCategory)(e))
                    }
                }))
            })))
        }
    },
    f2zl: function(e, t, a) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var r = function(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }(a("GiK3"));
        t.default = function(e) {
            var t = e.avatarUrl,
                a = e.name;
            return r.default.createElement("div", {
                className: "center relative z3 hover-z9"
            }, r.default.createElement("div", {
                className: "border-box bg-cover bg-center bg-no-repeat relative circle w16 h16 w17-md h17-md m-auto p1",
                style: {
                    backgroundImage: "url(" + t + ")"
                }
            }), r.default.createElement("p", {
                className: "bold type-12 lh6"
            }, a))
        }
    },
    "hzo/": function(e, t, a) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var r = function(e) {
                return e && e.__esModule ? e : {
                    default: e
                }
            }(a("GiK3")),
            l = a("WXYU"),
            n = a("Z5jf");
        t.default = function(e) {
            var t = e.backed,
                a = e.favoriteCategory,
                u = e.name,
                o = e.hoverColor;
            return r.default.createElement("div", {
                className: "border-box t2 t0-md l0 pt100p w18 w20-md mt2 mt1-md rounded-large pl3 pr3 click-through bg-" + o + " absolute hover-item-show-opacity " + (l.hoverColorCodes.dark.includes(o) ? "white" : "black"),
                style: {
                    transition: "all 0.18s ease-in-out"
                }
            }, r.default.createElement("p", {
                className: "bold type-12 lh1"
            }, u), r.default.createElement("hr", {
                className: "w2 h1 clear-both block border-1 border-" + (l.hoverColorCodes.dark.includes(o) ? "white" : "black")
            }), r.default.createElement("p", {
                className: "light type-12 mt-1 ml-1 mr-1"
            }, r.default.createElement(n.BoundTranslate, {
                i18nKey: "views.site.team.projects_backed"
            })), r.default.createElement("p", {
                className: "bold type-12 mt-2"
            }, t || r.default.createElement(n.BoundTranslate, {
                i18nKey: "views.site.team.none_yet"
            })), r.default.createElement("p", {
                className: "light type-12 mt-2 ml-1 mr-1"
            }, r.default.createElement(n.BoundTranslate, {
                i18nKey: "views.site.team.favorite_category"
            })), r.default.createElement("p", {
                className: "bold type-12 mt-2"
            }, a ? r.default.createElement(n.BoundTranslate, {
                i18nKey: "views.site.team." + a.replace(/\s/g, "_").toLowerCase()
            }) : null))
        }
    },
    jGUh: function(e, t, a) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var r = u(a("GiK3")),
            l = u(a("Vlye")),
            n = u(a("zYhx"));

        function u(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }
        t.default = function(e) {
            var t = e.state,
                a = e.dispatch;
            return r.default.createElement("div", {
                className: "border-box mt10"
            }, r.default.createElement(l.default, {
                state: t
            }), r.default.createElement(n.default, {
                state: t,
                dispatch: a
            }))
        }
    },
    qfkN: function(e, t, a) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var r = u(a("GiK3")),
            l = u(a("f2zl")),
            n = u(a("hzo/"));

        function u(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }
        t.default = function(e) {
            return r.default.createElement("div", {
                onTouchStart: function(e) {
                    e.stopPropagation()
                },
                className: "border-box relative align-middle mt1 pl1 pr1 pb1 pt2 w18 w20-md rounded-large center z3 hover-z9 hover-target-opacity hover-bg-" + e.hoverColor,
                style: {
                    transition: "all 0.18s ease-in-out"
                }
            }, r.default.createElement(l.default, e), r.default.createElement(n.default, e))
        }
    },
    zYhx: function(e, t, a) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var r = Object.assign || function(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var a = arguments[t];
                    for (var r in a) Object.prototype.hasOwnProperty.call(a, r) && (e[r] = a[r])
                }
                return e
            },
            l = o(a("GiK3")),
            n = o(a("qfkN")),
            u = o(a("IMut"));

        function o(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }
        t.default = function(e) {
            var t = e.state,
                a = e.dispatch,
                o = (0, u.default)(t.users, t.filters);
            return l.default.createElement("div", {
                className: "inline-flex flex-wrap justify-center items-start"
            }, o.map(function(e) {
                return l.default.createElement(n.default, r({
                    key: e.avatarUrl,
                    dispatch: a
                }, e))
            }))
        }
    }
}, ["/d1k"]);
//# sourceMappingURL=team.3dc7dabc76d8e17c634c.js.map