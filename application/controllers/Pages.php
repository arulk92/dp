<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php

class Pages extends CI_Controller
{
    function __construct(){
        parent::__construct();
        $this->load->helper('url');

        $this->data['base_url'] = $this->config->config['base_url'];
    }

    public function index(){
        $this->load->view('index', $this->data);
    }

    public function clients(){
        $this->load->view('clients', $this->data);
    }

    public function our_team(){
        $this->load->view('our_team', $this->data);
    }

    public function portfolio(){
        $this->load->view('portfolio', $this->data);
    }

}

?>