<!DOCTYPE html>
<!--[if lte IE 7]><html class="ie7" lang="en-GB" prefix="og: http://ogp.me/ns#"> <![endif]-->
<!--[if IE 8]><html class="ie8" lang="en-GB" prefix="og: http://ogp.me/ns#"> <![endif]-->
<!--[if IE 9]><html class="ie9" lang="en-GB" prefix="og: http://ogp.me/ns#"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en-GB" prefix="og: http://ogp.me/ns#">
<!--<![endif]-->

<head>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,800,700,900%7COpen+Sans:300italic,400italic,600italic,300,400,600" />
    <!--[if ie]><meta content='IE=edge' http-equiv='X-UA-Compatible'/><![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="revisit-after" content="7 DAYS">
    <meta name="robots" content="All">
    <link rel="shortcut icon" href="https://theme-one.com/the-grid/wp-content/uploads/sites/6/2015/09/the-grid-favicon.png" />
    <script>
        //if ( top !== self ) top.location.replace( self.location.href );
    </script>
    <script type="text/javascript">
        document.documentElement.className = document.documentElement.className + ' yes-js js_active js'
    </script>
    <title>Multi Rows Slider - The Grid</title>
    <style>
        .wishlist_table .add_to_cart,
        a.add_to_wishlist.button.alt {
            border-radius: 16px;
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px
        }
    </style>
    <link rel="canonical" href="https://theme-one.com/the-grid/multi-rows-slider/" />
    <meta property="og:locale" content="en_GB" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Multi Rows Slider - The Grid" />
    <meta property="og:url" content="https://theme-one.com/the-grid/multi-rows-slider/" />
    <meta property="og:site_name" content="The Grid" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="Multi Rows Slider - The Grid" />
    <link rel='dns-prefetch' href='//s.w.org' />
    <link rel="alternate" type="application/rss+xml" title="The Grid &raquo; Feed" href="https://theme-one.com/the-grid/feed/" />
    <link rel="alternate" type="application/rss+xml" title="The Grid &raquo; Comments Feed" href="https://theme-one.com/the-grid/comments/feed/" />
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline!important;
            border: none!important;
            box-shadow: none!important;
            height: 1em!important;
            width: 1em!important;
            margin: 0 .07em!important;
            vertical-align: -0.1em!important;
            background: none!important;
            padding: 0!important
        }
    </style>
    <style id='woocommerce-inline-inline-css' type='text/css'>
        .woocommerce form .form-row .required {
            visibility: visible
        }
    </style>
    <link rel='stylesheet' id='yith-wcwl-font-awesome-css' href='https://theme-one.com/the-grid/wp-content/plugins/yith-woocommerce-wishlist/assets/css/font-awesome.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='main-styles-css' href='https://theme-one.com/the-grid/wp-content/themes/grid-theme/style.css' type='text/css' media='all' />
    <link rel='stylesheet' id='the-grid-css' href='https://theme-one.com/the-grid/wp-content/plugins/the-grid/frontend/assets/css/the-grid.min.css' type='text/css' media='all' />
    <style id='the-grid-inline-css' type='text/css'>
        .tolb-holder {
            background: rgba(0, 0, 0, .8)
        }

        .tolb-holder .tolb-close,
        .tolb-holder .tolb-title,
        .tolb-holder .tolb-counter,
        .tolb-holder .tolb-next i,
        .tolb-holder .tolb-prev i {
            color: #fff
        }

        .tolb-holder .tolb-load {
            border-color: rgba(255, 255, 255, .2);
            border-left: 3px solid #fff
        }

        .to-heart-icon,
        .to-heart-icon svg,
        .to-post-like,
        .to-post-like .to-like-count {
            position: relative;
            display: inline-block
        }

        .to-post-like {
            width: auto;
            cursor: pointer;
            font-weight: 400
        }

        .to-heart-icon {
            float: left;
            margin: 0 4px 0 0
        }

        .to-heart-icon svg {
            overflow: visible;
            width: 15px;
            height: 14px
        }

        .to-heart-icon g {
            -webkit-transform: scale(1);
            transform: scale(1)
        }

        .to-heart-icon path {
            -webkit-transform: scale(1);
            transform: scale(1);
            transition: fill .4s ease, stroke .4s ease
        }

        .no-liked .to-heart-icon path {
            fill: #999;
            stroke: #999
        }

        .empty-heart .to-heart-icon path {
            fill: transparent!important;
            stroke: #999
        }

        .liked .to-heart-icon path,
        .to-heart-icon svg:hover path {
            fill: #ff6863!important;
            stroke: #ff6863!important
        }

        @keyframes heartBeat {
            0% {
                transform: scale(1)
            }
            20% {
                transform: scale(.8)
            }
            30% {
                transform: scale(.95)
            }
            45% {
                transform: scale(.75)
            }
            50% {
                transform: scale(.85)
            }
            100% {
                transform: scale(.9)
            }
        }

        @-webkit-keyframes heartBeat {
            0%,
            100%,
            50% {
                -webkit-transform: scale(1)
            }
            20% {
                -webkit-transform: scale(.8)
            }
            30% {
                -webkit-transform: scale(.95)
            }
            45% {
                -webkit-transform: scale(.75)
            }
        }

        .heart-pulse g {
            -webkit-animation-name: heartBeat;
            animation-name: heartBeat;
            -webkit-animation-duration: 1s;
            animation-duration: 1s;
            -webkit-animation-iteration-count: infinite;
            animation-iteration-count: infinite;
            -webkit-transform-origin: 50% 50%;
            transform-origin: 50% 50%
        }

        .to-post-like a {
            color: inherit!important;
            fill: inherit!important;
            stroke: inherit!important
        }
    </style>
    <link rel='stylesheet' id='wp-mediaelement-css' href='https://theme-one.com/the-grid/wp-content/plugins/the-grid/frontend/assets/css/wp-mediaelement.min.css' type='text/css' media='all' />
    <script type='text/javascript' src='https://theme-one.com/the-grid/wp-includes/js/jquery/jquery.js'></script>
    <script type='text/javascript' src='https://theme-one.com/the-grid/wp-includes/js/jquery/jquery-migrate.min.js'></script>
    <script type='text/javascript'>
        var mejsL10n = {
            "language": "en",
            "strings": {
                "mejs.install-flash": "You are using a browser that does not have Flash player enabled or installed. Please turn on your Flash player plugin or download the latest version from https:\/\/get.adobe.com\/flashplayer\/",
                "mejs.fullscreen-off": "Turn off Fullscreen",
                "mejs.fullscreen-on": "Go Fullscreen",
                "mejs.download-video": "Download Video",
                "mejs.fullscreen": "Fullscreen",
                "mejs.time-jump-forward": ["Jump forward 1 second", "Jump forward %1 seconds"],
                "mejs.loop": "Toggle Loop",
                "mejs.play": "Play",
                "mejs.pause": "Pause",
                "mejs.close": "Close",
                "mejs.time-slider": "Time Slider",
                "mejs.time-help-text": "Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.",
                "mejs.time-skip-back": ["Skip back 1 second", "Skip back %1 seconds"],
                "mejs.captions-subtitles": "Captions\/Subtitles",
                "mejs.captions-chapters": "Chapters",
                "mejs.none": "None",
                "mejs.mute-toggle": "Mute Toggle",
                "mejs.volume-help-text": "Use Up\/Down Arrow keys to increase or decrease volume.",
                "mejs.unmute": "Unmute",
                "mejs.mute": "Mute",
                "mejs.volume-slider": "Volume Slider",
                "mejs.video-player": "Video Player",
                "mejs.audio-player": "Audio Player",
                "mejs.ad-skip": "Skip ad",
                "mejs.ad-skip-info": ["Skip in 1 second", "Skip in %1 seconds"],
                "mejs.source-chooser": "Source Chooser",
                "mejs.stop": "Stop",
                "mejs.speed-rate": "Speed Rate",
                "mejs.live-broadcast": "Live Broadcast",
                "mejs.afrikaans": "Afrikaans",
                "mejs.albanian": "Albanian",
                "mejs.arabic": "Arabic",
                "mejs.belarusian": "Belarusian",
                "mejs.bulgarian": "Bulgarian",
                "mejs.catalan": "Catalan",
                "mejs.chinese": "Chinese",
                "mejs.chinese-simplified": "Chinese (Simplified)",
                "mejs.chinese-traditional": "Chinese (Traditional)",
                "mejs.croatian": "Croatian",
                "mejs.czech": "Czech",
                "mejs.danish": "Danish",
                "mejs.dutch": "Dutch",
                "mejs.english": "English",
                "mejs.estonian": "Estonian",
                "mejs.filipino": "Filipino",
                "mejs.finnish": "Finnish",
                "mejs.french": "French",
                "mejs.galician": "Galician",
                "mejs.german": "German",
                "mejs.greek": "Greek",
                "mejs.haitian-creole": "Haitian Creole",
                "mejs.hebrew": "Hebrew",
                "mejs.hindi": "Hindi",
                "mejs.hungarian": "Hungarian",
                "mejs.icelandic": "Icelandic",
                "mejs.indonesian": "Indonesian",
                "mejs.irish": "Irish",
                "mejs.italian": "Italian",
                "mejs.japanese": "Japanese",
                "mejs.korean": "Korean",
                "mejs.latvian": "Latvian",
                "mejs.lithuanian": "Lithuanian",
                "mejs.macedonian": "Macedonian",
                "mejs.malay": "Malay",
                "mejs.maltese": "Maltese",
                "mejs.norwegian": "Norwegian",
                "mejs.persian": "Persian",
                "mejs.polish": "Polish",
                "mejs.portuguese": "Portuguese",
                "mejs.romanian": "Romanian",
                "mejs.russian": "Russian",
                "mejs.serbian": "Serbian",
                "mejs.slovak": "Slovak",
                "mejs.slovenian": "Slovenian",
                "mejs.spanish": "Spanish",
                "mejs.swahili": "Swahili",
                "mejs.swedish": "Swedish",
                "mejs.tagalog": "Tagalog",
                "mejs.thai": "Thai",
                "mejs.turkish": "Turkish",
                "mejs.ukrainian": "Ukrainian",
                "mejs.vietnamese": "Vietnamese",
                "mejs.welsh": "Welsh",
                "mejs.yiddish": "Yiddish"
            }
        };
    </script>
    <script type='text/javascript' src='https://theme-one.com/the-grid/wp-includes/js/mediaelement/mediaelement-and-player.min.js'></script>
    <script type='text/javascript' src='https://theme-one.com/the-grid/wp-includes/js/mediaelement/mediaelement-migrate.min.js'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var _wpmejsSettings = {
            "pluginPath": "\/the-grid\/wp-includes\/js\/mediaelement\/",
            "classPrefix": "mejs-",
            "stretching": "responsive"
        }; /* ]]> */
    </script>
    <link rel='https://api.w.org/' href='https://theme-one.com/the-grid/wp-json/' />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://theme-one.com/the-grid/xmlrpc.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://theme-one.com/the-grid/wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 4.9.8" />
    <meta name="generator" content="WooCommerce 3.4.4" />
    <link rel='shortlink' href='https://theme-one.com/the-grid/?p=771' />
    <link rel="alternate" type="application/json+oembed" href="https://theme-one.com/the-grid/wp-json/oembed/1.0/embed?url=https%3A%2F%2Ftheme-one.com%2Fthe-grid%2Fmulti-rows-slider%2F" />
    <link rel="alternate" type="text/xml+oembed" href="https://theme-one.com/the-grid/wp-json/oembed/1.0/embed?url=https%3A%2F%2Ftheme-one.com%2Fthe-grid%2Fmulti-rows-slider%2F&#038;format=xml" />
    <style type="text/css"></style>
    <style type="text/css">
        .to-lb-overlay,
        #search-overlay {
            background: #fff
        }

        .website-logo img {
            min-height: 35%;
            max-height: 35%
        }

        @media screen and (max-width:480px) {
            .website-logo img {
                min-height: 28%;
                max-height: 28%
            }
        }

        #single-post-section article a,
        #single-portfolio-section article a {
            color: #ff6863
        }

        #single-post-section article a:hover,
        #single-portfolio-section article a:hover {
            color: initial!important
        }

        .headerColor {
            background: #fff;
            color: #646464
        }

        #header .cart-counter *,
        #header .wishlist-link *,
        #header-container input.search,
        .search-close .fa-times {
            color: #646464
        }

        .headerColor2 {
            background: #646464;
            color: #fff
        }

        .header-mobile .sliding-sidebar-open,
        .search-button.no-nav {
            color: #646464
        }

        .trans.dark .sliding-menu-open *,
        .trans.dark .sliding-sidebar-open * {
            fill: #999
        }

        .trans.light .sliding-menu-open *,
        .trans.light .sliding-sidebar-open * {
            fill: #e8e8e8
        }

        #to-slider {
            background: #292929
        }

        .sidebarColor {
            background: #323943
        }

        #sliding-menu {
            background: #323943
        }

        #sliding-menu-overlay {
            background: #323943;
            opacity: 1
        }

        #left-nav {
            background: #323943
        }

        .dark #left-nav>ul>li>a,
        .dark #left-nav li:hover>a,
        .dark #left-nav li:hover>a i,
        .dark #left-menu-shop li a,
        .dark #left-nav li.back-sub-menu span,
        .dark #left-nav li.back-sub-menu i,
        .dark #left-nav .left-cart-counter:hover *,
        .dark #left-nav .to-wishlist-counter:hover * {
            color: #59585b
        }

        .light #left-nav>ul>li>a,
        .light #left-nav li:hover>a,
        .light #left-nav li:hover>a i,
        .light #left-menu-shop li a,
        .light #left-nav li.back-sub-menu span,
        .light #left-nav li.back-sub-menu i,
        .light #left-nav .left-cart-counter:hover *,
        .light #left-nav .to-wishlist-counter:hover * {
            color: #fff
        }

        .sliding-menu-open *,
        .sliding-sidebar-open * {
            fill: #646464
        }

        #left-nav ul li:before {
            background: #ddd
        }

        #left-nav ul li.current-menu-item:before,
        #left-nav ul li.current_page_ancestor:before {
            background: #2fbfc1
        }

        body.dark,
        .dark p,
        .dark div,
        .dark a,
        #header .buttons a,
        #header .quantity {
            color: #999
        }

        .dark h1,
        .dark h2,
        .dark h3,
        .dark h4,
        .dark h5,
        .dark h6,
        .dark h1 a,
        .dark h2 a,
        .dark h3 a,
        .dark h4 a,
        .dark h5 a,
        .dark h6 a,
        .dark.to-slide .to-slide-content-inner *,
        .dark dt,
        .dark .post-title a,
        .dark .summary .posted_in,
        #header .widget_shopping_cart_content .cart_list a,
        .dark table.cart td.product-name a,
        .dark .woocommerce-tabs ul.tabs li,
        .dark .comment-list .comment-author,
        .dark .comment-list .comment-author a,
        .dark .to-pie-chart span,
        .dark .to-progress-bar-title strong,
        .dark .to-counter-number span,
        .dark .to-counter-number-desc,
        .dark .to-sc-twitter-icon,
        .dark li.active-tab {
            color: #59585b
        }

        .dark .widget a {
            color: #59585b
        }

        .dark .widget a:hover {
            color: #2fbfc1
        }

        .dark.to-page-heading-img-true * {
            color: #59585b
        }

        .dark .to-grid-filters-button svg * {
            fill: #999
        }

        .dark .post-info .post-date .date {
            color: #59585b
        }

        .dark .post-info .post-date .month {
            color: #79787A
        }

        .dark .post-info .post-date .year {
            color: #999
        }

        .dark .owl-page.active span,
        .dark .owl-page:hover span {
            background: #999
        }

        .dark .isotope-pages li.active,
        .dark .isotope-pages li:hover {
            background: #999999!important
        }

        body.light,
        .light p,
        .light div,
        .light a {
            color: #e8e8e8
        }

        .light h1,
        .light h2,
        .light h3,
        .light h4,
        .light h5,
        .light h6,
        .light h1 a,
        .light h2 a,
        .light h3 a,
        .light h4 a,
        .light h5 a,
        .light h6 a,
        .light.to-slide .to-slide-content-inner *,
        .light dt,
        .light .post-title a,
        .light .summary .posted_in,
        .light table.cart td.product-name a,
        .light .woocommerce-tabs ul.tabs li,
        .light .comment-list .comment-author,
        .light .comment-list .comment-author a,
        .light .to-pie-chart span,
        .light .to-progress-bar-title strong,
        .light .to-counter-number span,
        .light .to-counter-number-desc,
        .light .to-sc-twitter-icon,
        .light li.active-tab {
            color: #fff
        }

        .light .widget a {
            color: #FCFCFC
        }

        .light .widget a:hover {
            color: #2fbfc1
        }

        .light.to-page-heading-img-true * {
            color: #fff
        }

        .light .to-grid-filters-button svg * {
            fill: #e8e8e8
        }

        .light .post-info .post-date .date {
            color: #fff
        }

        .light .post-info .post-date .month {
            color: #F3F3F3
        }

        .light .post-info .post-date .year {
            color: #e8e8e8
        }

        .light .owl-page.active span,
        .light .owl-page:hover span {
            background: #e8e8e8
        }

        .light .isotope-pages li.active,
        .light .isotope-pages li:hover {
            background: #e8e8e8!important
        }

        .light.to-page-heading .subtitle {
            color: #f3f5f8
        }

        body,
        #body-overlay,
        #outer-container,
        #inner-container,
        .comment-list li,
        .comment-list li.comment>#respond {
            background: #3CB878
        }

        #preloader,
        #header .widget_shopping_cart .cart_list li:hover,
        #to-crumbs-overlay,
        #to-author-bio-overlay,
        .to-grid-filter-overlay,
        code,
        .wp-caption,
        .to-team-carousel.circle .to-member-social li,
        .to-progress-bar-holder {
            background: #F5F6FA
        }

        .grid-home-page .next-container {
            background: #F5F6FA!important
        }

        .mejs-controls,
        .to-audio-player,
        #current-player-mini {
            background: #2B2D2F;
            color: #fff
        }

        .to-item.blog .to-item-cat:hover {
            background: #2B2D2F!important;
            color: #ffffff!important
        }

        .to-audio-player i,
        .to-audio-player span,
        .mejs-controls .mejs-playpause-button,
        .mejs-controls .mejs-button button,
        .mejs-controls .mejs-currenttime,
        .mejs-controls .mejs-duration {
            color: #ffffff!important
        }

        .accentBg,
        .accentBgHover:hover,
        .mejs-time-current,
        .mejs-volume-current,
        .mejs-horizontal-volume-current,
        div.wpcf7-response-output,
        .woocommerce-info,
        .woocommerce-info *,
        .woocommerce .single_add_to_cart_button,
        .wishlist_table .add_to_cart,
        .woocommerce .woocommerce-message,
        .woocommerce .woocommerce-message a,
        .woocommerce .woocommerce-error,
        .woocommerce .woocommerce-error a,
        .woocommerce .product-wrap:hover .product_type_simple:hover,
        .woocommerce .product-wrap:hover .add_to_cart_button:hover,
        .woocommerce .widget_layered_nav_filters ul li a:hover,
        .woocommerce-page .widget_layered_nav_filters ul li a:hover,
        .widget_price_filter .ui-slider .ui-slider-range,
        #slider_per_page .ui-slider-range,
        select option:hover,
        .chosen-container ul.chosen-results li.highlighted,
        .top-header-button,
        .active {
            background: #2fbfc1!important;
            color: #ffffff!important
        }

        ::selection {
            background: #2fbfc1!important;
            color: #ffffff!important
        }

        ::-moz-selection {
            background: #2fbfc1!important;
            color: #ffffff!important
        }

        .woocommerce .single_add_to_cart_button,
        .wishlist_table .add_to_cart {
            border-color: #2fbfc1!important
        }

        .csstransforms .loading {
            border-left: 3px solid rgba(47, 191, 193, .2);
            border-right: 3px solid rgba(47, 191, 193, .2);
            border-bottom: 3px solid rgba(47, 191, 193, .2);
            border-top: 3px solid #2fbfc1
        }

        .csstransforms .next-container.load {
            border-top: 2px solid #2fbfc1
        }

        .to-item.active {
            background: none!important
        }

        .svg-holder svg * {
            stroke: #2fbfc1
        }

        .button:hover,
        input[type=submit]:hover,
        input[type="button"]:hover,
        input[type=submit]:hover,
        .header-pages li.active,
        #header .buttons a:hover,
        #header .total .amount,
        #header .widget_shopping_cart_content .cart_list a:hover,
        .grid-filter-title:hover,
        .no-touch .isotopeFilters-title:hover,
        .grid-home-page .next-container:hover,
        .to-grid-filter-title.actived,
        .to-item-overlay,
        .widget .tagcloud a:hover,
        .light .widget.widget_tag_cloud a:hover,
        .post-tags a:hover,
        .sliding-menu-open:hover>div,
        .widget.widget_tag_cloud a:hover,
        .comment-list .reply a:hover,
        .dark .comment-list .comment-author a:hover,
        input[type=submit]:hover,
        button[type=submit]:hover,
        .to-page-nav li.active a,
        .to-page-nav li a:hover,
        .page-numbers .current,
        .page-numbers a:hover,
        .accentColor,
        .accentColorHover:hover,
        a.accentColor,
        a:not(.to-button):hover,
        .required,
        input[type=checkbox]:checked:before,
        input[type=radio]:checked:before,
        #header-container.trans.light .search-button:hover .fa-search,
        #header-container.trans.dark .search-button:hover .fa-search,
        .widget.widget_rss cite,
        .widget.widget_calendar #today,
        .required,
        .wpcf7-form .wpcf7-not-valid-tip,
        #comment-status,
        .comment-awaiting-moderation,
        .comment-list .comment-meta a:hover,
        #cancel-comment-reply-link:hover,
        #to-crumbs a:hover,
        #portfolio-all-items:hover i,
        .to-item-meta a:hover *,
        .to-excerpt-more:hover,
        .to-search-item-content h4 a:hover,
        .to-testimonial .to-testimonial-autor-desc,
        .to-team-carousel .to-member-social li a,
        .to-quote .fa-quote-left,
        .post-like:hover i,
        .post-like.liked,
        .post-like.liked i,
        .dark .post-like.liked,
        .dark .widget a.post-like.no-liked:hover i,
        .post-like .icon-to-x,
        .ui-menu-item:hover .title,
        .grid-home-page .to-grid-filter-title:hover,
        .blog-page .to-grid-filter-title:hover,
        .portfolio-page .to-grid-filter-title:hover,
        .no-touch .to-item.blog h2:hover,
        .no-touch .to-item.blog .to-item-author:hover,
        .no-touch .to-item.blog .to-item-comments:hover,
        .to-masonry .to-item.tall .to-excerpt-masonry:hover,
        .to-item-title-hover,
        .to-item-dot:before,
        .to-item-dot .before,
        .single-product-summary .amount,
        #comment-status p,
        .widget .total .amount,
        .woocommerce ul.products li.product h3:hover,
        a.woocommerce-review-link,
        a.woocommerce-review-link .count,
        .woocommerce ul.products li.product .price .amount,
        .woocommerce-page ul.products li.product .price .amount,
        .woocommerce div.product .stock,
        .woocommerce-page div.product .stock,
        tbody .cart_item .product-name,
        .order-total .amount,
        .lost_password a,
        .posted_in a,
        .post-cat-holder a,
        .post-tag-holder a,
        .actived {
            color: #2fbfc1
        }

        .to-item.blog.quote.center h2:hover,
        .to-item.blog.link.center h2:hover,
        .woocommerce-tabs .tabs li.active,
        .woocommerce-tabs .tabs li:hover,
        .no-menu-assigned:hover,
        #current-player.close .mejs-playpause-button:hover,
        .tweet-link-color,
        .stars .active,
        .featured-link,
        .featured .to-ptable-header h5 {
            color: #2fbfc1!important
        }

        .to-search-item .to-excerpt-more:hover {
            color: #2fbfc1!important
        }

        .sliding-menu-open:hover *,
        .sliding-sidebar-open:hover *,
        #left-menu-button:hover * {
            fill: #2fbfc1!important
        }

        #call-to-action:hover *,
        #left-menu-button:hover * {
            color: #ffffff!important
        }

        .grid-home-page .to-grid-filter-title:hover,
        .blog-page .to-grid-filter-title:hover,
        .portfolio-page .to-grid-filter-title:hover {
            color: #2fbfc1!important
        }

        .to-item .to-item-overlay,
        .to-item.blog .to-item-wrapper,
        .to-item.blog .to-item-social {
            background: #fff
        }

        .to-item.blog .to-item-content h2 {
            color: #59585b
        }

        .to-item .excerpt,
        .to-item .to-item-social,
        .to-item.portfolio:not(.portstyle2) .to-item-cats {
            color: #999
        }

        .to-item.blog .to-item-cat {
            background: #2fbfc1
        }

        .to-item.blog.center:not(.portstyle2) .to-item-information,
        .to-item.blog.center:not(.portstyle2) .to-item-information a {
            color: #e4e4e4
        }

        .to-item.portfolio:not(.portstyle2) .to-item-wrapper,
        .to-item.portfolio:not(.portstyle2) h2,
        .to-item.portfolio:not(.portstyle2) h2 a {
            color: #59585b
        }

        .to-item.center:not(.portstyle2) .to-item-content,
        .to-item.blog.center h2 a {
            color: #ffffff!important
        }

        .to-item.blog.quote.center h2,
        .to-item.blog.link.center h2 {
            color: #59585b!important
        }

        .to-item.portfolio h2:hover,
        .to-item.portfolio a:hover {
            color: #2fbfc1!important
        }

        .to-item .to-item-lightbox-link,
        .to-item .to-item-lightbox-link i,
        .to-item .to-item-content-link,
        .to-item .to-item-content-link i,
        .to-item .to-item-audio-link,
        .to-item .to-item-audio-link i {
            color: #999
        }

        .to-item.portfolio .to-item-lightbox-link:hover,
        .to-item.portfolio .to-item-lightbox-link:hover i,
        .to-item.portfolio .to-item-content-link:hover,
        .to-item.portfolio .to-item-content-link:hover i,
        .to-item.portfolio .to-item-audio-link:hover,
        .to-item.portfolio .to-item-audio-link:hover i {
            color: #2fbfc1
        }

        .to-item.blog.quote .to-item-content,
        .to-item.blog.link .to-item-content {
            background: #fff
        }

        .blog-grid-fullwidth .to-item.blog.quote .to-item-content,
        .blog-grid-fullwidth .to-item.blog.link .to-item-content,
        .grid-home-page .to-item.blog.quote .to-item-content,
        .grid-home-page .to-item.blog.link .to-item-content {
            background: #f3f5f8
        }

        .ui-autocomplete {
            background: #191919!important
        }

        .ui-menu,
        .ui-menu .ui-menu-item,
        .image-autocomplete {
            background: rgba(31, 31, 31, .95);
            color: #EBEBEB
        }

        .ui-menu-item .desc {
            color: #C1C1C1
        }

        .ui-menu-item .ui-corner-all:hover,
        .ui-menu-item .ui-corner-all.ui-state-focus {
            background: rgba(0, 0, 0, .25)!important
        }

        .ui-menu-item .fa-pencil {
            background: rgba(0, 0, 0, .15)
        }

        #header-container.trans.light #top-nav>ul>li,
        #header-container.trans.light .sliding-sidebar-open,
        #header-container.trans.light .search-button .fa-search,
        #header.trans.light .to-wishlist-counter i,
        #header.trans.light .to-wishlist-counter span,
        #header.trans.light .cart-counter i,
        #header.trans.light .cart-counter span {
            color: #fff
        }

        #header-container.trans.light .sliding-menu-open *,
        #header-container.trans.light .sliding-sidebar-open * {
            fill: #fff
        }

        #header-container.trans.dark #top-nav>ul>li,
        #header-container.trans.dark .sliding-sidebar-open,
        #header-container.trans.dark .search-button .fa-search,
        #header.trans.dark .to-wishlist-counter i,
        #header.trans.dark .to-wishlist-counter span,
        #header.trans.dark .cart-counter i,
        #header.trans.dark .cart-counter span {
            color: #313131
        }

        #header-container.trans.dark .sliding-menu-open *,
        #header-container.trans.dark .sliding-sidebar-open * {
            fill: #313131
        }

        #header .widget_shopping_cart,
        .woocommerce .cart-notification,
        #header .wishlist-notification,
        #header .cart-notification,
        #top-nav .sub-menu {
            background: #fff;
            color: #999
        }

        #top-nav>ul>.megamenu>.sub-menu>li>a,
        #top-nav .sub-menu li:hover>a {
            color: #59585b
        }

        #top-nav ul ul .current-menu-item>a,
        #top-nav>ul>.megamenu>.sub-menu>li:hover>a,
        #top-nav>ul>.megamenu>.sub-menu>li.current-menu-item:hover>a {
            color: #2fbfc1!important
        }

        #top-nav>ul>.megamenu>.sub-menu>li.current-menu-item>a {
            color: inherit!important
        }

        #top-nav>ul>li>a.hover {
            border-color: #2fbfc1
        }

        .dark *,
        .dark table tbody tr td:first-child,
        .dark .widget.widget_pages li a,
        .dark .widget.widget_nav_menu li a {
            border-color: #ddd
        }

        .light *,
        .light table tbody tr td:first-child,
        .light .widget.widget_pages li a,
        .light .widget.widget_nav_menu li a {
            border-color: #ddd
        }

        .comment-list .parent:after,
        .comment-list .children:before,
        .comment-list .children:after {
            border-color: #f3f5f8
        }

        #sliding-sidebar .widget .no-post-like-image {
            background-color: #292F37!important
        }

        .button,
        input[type="button"],
        input[type=submit],
        .header-pages li,
        .grid-filter-title,
        .no-touch .isotopeFilters-title,
        .grid-home-page .next-container,
        .to-grid-filter-title,
        .to-item-overlay,
        .widget .tagcloud a,
        .light .widget.widget_tag_cloud a,
        .post-tags a,
        .sliding-menu-open:hover>div,
        .widget.widget_tag_cloud a,
        .comment-list .reply a,
        input[type=submit],
        button[type=submit],
        .to-page-nav li a,
        .to-page-nav li a {
            color: inherit
        }

        .light .to-icon.full-bg {
            background: #3A3A3A
        }

        .dark .to-icon.full-bg {
            background: #FCFCFC
        }

        .woocommerce ul.products li.product h3:hover,
        .woocommerce .product-desc-inner .button,
        .woocommerce .product-desc-inner .button:hover {
            color: #59585b!important
        }
    </style>
    <noscript>
        <style>
            .woocommerce-product-gallery {
                opacity: 1!important
            }
        </style>
    </noscript>
    <style type="text/css" id="wp-custom-css">
        Auxerunt haec vulgi sordidioris audaciam,
        quod cum ingravesceret penuria commeatuum,
        famis et furoris inpulsu Eubuli cuiusdam inter suos clari domum ambitiosam ignibus subditis inflammavit rectoremque ut sibi iudicio imperiali addictum calcibus incessens et pugnis conculcans seminecem laniatu miserando discerpsit. post cuius lacrimosum interitum in unius exitio quisque imaginem periculi sui considerans documento recenti similia formidabat. Postremo ad id indignitatis est ventum,
        ut cum peregrini ob formidatam haut ita dudum alimentorum inopiam pellerentur ab urbe praecipites,
        sectatoribus disciplinarum liberalium inpendio paucis sine respiratione ulla extrusis,
        tenerentur minimarum adseclae veri,
        quique id simularunt ad tempus,
        et tria milia saltatricum ne interpellata quidem cum choris totidemque remanerent magistris. Quam ob rem circumspecta cautela observatum est deinceps et cum edita montium petere coeperint grassatores,
        loci iniquitati milites cedunt. ubi autem in planitie potuerint reperiri,
        quod contingit adsidue,
        nec exsertare lacertos nec crispare permissi tela,
        quae vehunt bina vel terna,
        pecudum ritu inertium trucidantur. Apud has gentes,
        quarum exordiens initium ab Assyriis ad Nili cataractas porrigitur et confinia Blemmyarum,
        omnes pari sorte sunt bellatores seminudi coloratis sagulis pube tenus amicti,
        equorum adiumento pernicium graciliumque camelorum per diversa se raptantes,
        in tranquillis vel turbidis rebus:nec eorum quisquam aliquando stivam adprehendit vel arborem colit aut arva subigendo quaeritat victum,
        sed errant semper per spatia longe lateque distenta sine lare sine sedibus fixis aut legibus:nec idem perferunt diutius caelum aut tractus unius soli illis umquam placet.
    </style>
</head>

<body class="page-template-default page page-id-771 dark woocommerce-no-js" style="background-color: #ee1a59;">
<div id="wrapper-scroll">
    <div id="textarea-scroll"></div>
</div>
<div id="sliding-sidebar" class="sidebarColor light">
    <div id="sliding-sidebar-inner">
        <div id="text-2" class="widget widget_text">
            <h4>The Grid &#8211; Wordpress Grid Plugin</h4>
            <div class="textwidget">
                <p style="font-size: 13px; line-height: 18px;padding-top: 10px;">The grid is a Wordpress grid plugin developed by professional coders and designers. We spent several months to produce the highest quality product.</p>
                <p style="font-size: 13px; line-height: 18px;">We especially took care about the loading speed performance by optimizing the queries and we also creating a custom and lightweight jQuery script.</p>
                <p style="font-size: 13px; line-height: 18px;">Possibilities are really infinite and the combinations will offer to you the perfect way to display and show off your articles, portfolio, products or any other custom post types.</p>
                <br>
            </div>
        </div>
        <div id="text-3" class="widget widget_text">
            <h4>Check-out our Mobius Theme</h4>
            <div class="textwidget">
                <a href="https://themeforest.net/item/mobius-responsive-multipurpose-wordpress-theme/8467936" target="_blank"><img alt="Mobius - Responsive Multi-Purpose WordPress Theme - Creative WordPress" src="https://theme-one.com/the-grid/wp-content/uploads/sites/6/2015/09/mobius-image-preview.png"></a>
            </div>
        </div>
    </div>
</div>
<div id="sliding-menu" class="light">
    <div id="sliding-menu-overlay"></div>
    <div id="sliding-menu-inner">
        <div id="sliding-menu-logo">
            <a href="https://theme-one.com/the-grid"><img class="to-loader-logo" src="https://theme-one.com/the-grid/wp-content/uploads/sites/6/2015/09/the-grid-logo-min.png" alt="" /></a>
        </div>
        <nav id="left-nav">
            <ul class="left-menu">
                <li id="menu-item-249" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-249"><a href="https://theme-one.com/the-grid/" class="menu-link-noparent">Home</a></li>
                <li id="menu-item-273" class="megamenu menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-273"><a href="https://theme-one.com/the-grid/suva/" class="menu-link-parent" data-depth="1">Masonry Layout<span class="arrow-indicator"><i class="tg-icon-arrow-next-thin"></i></span></a>
                    <ul class="sub-menu">
                        <li class="back-sub-menu" data-depth="0"><i class='tg-icon-arrow-prev-thin'></i><span>Back</span></li>
                        <li id="menu-item-258" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-258"><a href="https://theme-one.com/the-grid/kampala/" class="menu-link-noparent">Kampala<span class="menu-label">Trendy</span></a></li>
                        <li id="menu-item-259" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-259"><a href="https://theme-one.com/the-grid/lima/" class="menu-link-noparent">Lima</a></li>
                        <li id="menu-item-263" class="color-blue menu-item menu-item-type-post_type menu-item-object-page menu-item-263"><a href="https://theme-one.com/the-grid/maren/" class="menu-link-noparent"><i class="icon-menu fa color-blue"></i>Maren<span class="menu-label">Ajax Page</span></a></li>
                        <li id="menu-item-265" class="color-orange menu-item menu-item-type-post_type menu-item-object-page menu-item-265"><a href="https://theme-one.com/the-grid/panama/" class="menu-link-noparent"><i class="icon-menu fa color-orange"></i>Panama<span class="menu-label">fullwidth</span></a></li>
                        <li id="menu-item-267" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-267"><a href="https://theme-one.com/the-grid/praia/" class="menu-link-noparent">Praia<span class="menu-label">trendy</span></a></li>
                        <li id="menu-item-268" class="color-green menu-item menu-item-type-post_type menu-item-object-page menu-item-268"><a href="https://theme-one.com/the-grid/quito/" class="menu-link-noparent"><i class="icon-menu fa color-green"></i>Quito<span class="menu-label">Ajax Scroll</span></a></li>
                        <li id="menu-item-269" class="color-grey menu-item menu-item-type-post_type menu-item-object-page menu-item-269"><a href="https://theme-one.com/the-grid/riga/" class="menu-link-noparent"><i class="icon-menu fa color-grey"></i>Riga<span class="menu-label">slider</span></a></li>
                        <li id="menu-item-270" class="color-purple menu-item menu-item-type-post_type menu-item-object-page menu-item-270"><a href="https://theme-one.com/the-grid/sanaa/" class="menu-link-noparent"><i class="icon-menu fa color-purple"></i>Sanaa<span class="menu-label">woocommerce</span></a></li>
                    </ul>
                </li>
                <li id="menu-item-274" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-274"><a href="https://theme-one.com/the-grid/suva/" class="menu-link-parent" data-depth="1">Grid Layout<span class="arrow-indicator"><i class="tg-icon-arrow-next-thin"></i></span></a>
                    <ul class="sub-menu">
                        <li class="back-sub-menu" data-depth="0"><i class='tg-icon-arrow-prev-thin'></i><span>Back</span></li>
                        <li id="menu-item-250" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-250"><a href="https://theme-one.com/the-grid/apia/" class="menu-link-noparent">Apia</a></li>
                        <li id="menu-item-251" class="color-orange menu-item menu-item-type-post_type menu-item-object-page menu-item-251"><a href="https://theme-one.com/the-grid/brasilia/" class="menu-link-noparent"><i class="icon-menu fa color-orange"></i>Brasilia<span class="menu-label">FullWidth</span></a></li>
                        <li id="menu-item-252" class="color-blue menu-item menu-item-type-post_type menu-item-object-page menu-item-252"><a href="https://theme-one.com/the-grid/camberra/" class="menu-link-noparent"><i class="icon-menu fa color-blue"></i>Camberra<span class="menu-label">Ajax</span></a></li>
                        <li id="menu-item-253" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-253"><a href="https://theme-one.com/the-grid/caracas/" class="menu-link-noparent">Caracas</a></li>
                        <li id="menu-item-257" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-257"><a href="https://theme-one.com/the-grid/honiara/" class="menu-link-noparent">Honiara<span class="menu-label">Trendy</span></a></li>
                        <li id="menu-item-260" class="color-grey menu-item menu-item-type-post_type menu-item-object-page menu-item-260"><a href="https://theme-one.com/the-grid/lome/" class="menu-link-noparent"><i class="icon-menu fa color-grey"></i>Lome<span class="menu-label">Slider</span></a></li>
                        <li id="menu-item-261" class="color-green menu-item menu-item-type-post_type menu-item-object-page menu-item-261"><a href="https://theme-one.com/the-grid/malabo/" class="menu-link-noparent"><i class="icon-menu fa color-green"></i>Malabo<span class="menu-label">Ajax Scroll</span></a></li>
                        <li id="menu-item-262" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-262"><a href="https://theme-one.com/the-grid/male/" class="menu-link-noparent">Male<span class="menu-label">Trendy</span></a></li>
                        <li id="menu-item-264" class="color-grey menu-item menu-item-type-post_type menu-item-object-page menu-item-264"><a href="https://theme-one.com/the-grid/oslo/" class="menu-link-noparent"><i class="icon-menu fa color-grey"></i>Oslo<span class="menu-label">Slider</span></a></li>
                        <li id="menu-item-266" class="color-green menu-item menu-item-type-post_type menu-item-object-page menu-item-266"><a href="https://theme-one.com/the-grid/pracia/" class="menu-link-noparent"><i class="icon-menu fa color-green"></i>Pracia<span class="menu-label">Ajax Page</span></a></li>
                        <li id="menu-item-271" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-271"><a href="https://theme-one.com/the-grid/sofia/" class="menu-link-noparent">Sofia<span class="menu-label">Trendy</span></a></li>
                        <li id="menu-item-272" class="color-purple menu-item menu-item-type-post_type menu-item-object-page menu-item-272"><a href="https://theme-one.com/the-grid/suva/" class="menu-link-noparent"><i class="icon-menu fa color-purple"></i>Suva<span class="menu-label">woocommerce</span></a></li>
                    </ul>
                </li>
                <li id="menu-item-973" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-973"><a href="https://theme-one.com/the-grid/roma/" class="menu-link-parent" data-depth="1">Justified Layout<span class="arrow-indicator"><i class="tg-icon-arrow-next-thin"></i></span></a>
                    <ul class="sub-menu">
                        <li class="back-sub-menu" data-depth="0"><i class='tg-icon-arrow-prev-thin'></i><span>Back</span></li>
                        <li id="menu-item-975" class="color-blue menu-item menu-item-type-post_type menu-item-object-page menu-item-975"><a href="https://theme-one.com/the-grid/roma/" class="menu-link-noparent"><i class="icon-menu fa color-blue"></i>Roma<span class="menu-label">Ajax</span></a></li>
                        <li id="menu-item-974" class="color-orange menu-item menu-item-type-post_type menu-item-object-page menu-item-974"><a href="https://theme-one.com/the-grid/lisboa/" class="menu-link-noparent"><i class="icon-menu fa color-orange"></i>Lisboa<span class="menu-label">FULLWIDTH</span></a></li>
                        <li id="menu-item-1042" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1042"><a href="https://theme-one.com/the-grid/justified-sofia/" class="menu-link-noparent">Sofia<span class="menu-label">Trendy</span></a></li>
                        <li id="menu-item-1023" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1023"><a href="https://theme-one.com/the-grid/justified-caracas/" class="menu-link-noparent">Caracas<span class="menu-label">Classical</span></a></li>
                        <li id="menu-item-1007" class="color-grey menu-item menu-item-type-post_type menu-item-object-page menu-item-1007"><a href="https://theme-one.com/the-grid/justified-alofi/" class="menu-link-noparent"><i class="icon-menu fa color-grey"></i>Alofi<span class="menu-label">Slider Multirow</span></a></li>
                        <li id="menu-item-1006" class="color-grey menu-item menu-item-type-post_type menu-item-object-page menu-item-1006"><a href="https://theme-one.com/the-grid/justified-apia/" class="menu-link-noparent"><i class="icon-menu fa color-grey"></i>Apia<span class="menu-label">Slider</span></a></li>
                        <li id="menu-item-1043" class="color-green menu-item menu-item-type-post_type menu-item-object-page menu-item-1043"><a href="https://theme-one.com/the-grid/justified-male/" class="menu-link-noparent"><i class="icon-menu fa color-green"></i>Male<span class="menu-label">Ajax Scroll</span></a></li>
                        <li id="menu-item-1024" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1024"><a href="https://theme-one.com/the-grid/justified-dacca/" class="menu-link-noparent">Dacca<span class="menu-label">Trendy</span></a></li>
                    </ul>
                </li>
                <li id="menu-item-759" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children menu-item-759"><a href="https://theme-one.com/the-grid/slider-3d/" class="menu-link-parent" data-depth="1">Media Gallery<span class="arrow-indicator"><i class="tg-icon-arrow-next-thin"></i></span></a>
                    <ul class="sub-menu">
                        <li class="back-sub-menu" data-depth="0"><i class='tg-icon-arrow-prev-thin'></i><span>Back</span></li>
                        <li id="menu-item-760" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-760"><a href="https://theme-one.com/the-grid/grid-example/" class="menu-link-noparent">Grid Example</a></li>
                        <li id="menu-item-768" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-768"><a href="https://theme-one.com/the-grid/masonry-example-2/" class="menu-link-noparent">Masonry Example</a></li>
                        <li id="menu-item-761" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-761"><a href="https://theme-one.com/the-grid/slider-3d/" class="menu-link-noparent">Slider 3D<span class="menu-label">HOT</span></a></li>
                        <li id="menu-item-774" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-771 current_page_item menu-item-774"><a href="https://theme-one.com/the-grid/multi-rows-slider/" class="menu-link-noparent">Multi Rows Slider</a></li>
                        <li id="menu-item-900" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-900"><a href="https://theme-one.com/the-grid/dacca/" class="menu-link-noparent">Parallax effect<span class="menu-label">HOT</span></a></li>
                    </ul>
                </li>
                <li id="menu-item-869" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-869"><a href="https://theme-one.com/the-grid/vimeo-3/" class="menu-link-parent" data-depth="1">Social Media<span class="arrow-indicator"><i class="tg-icon-arrow-next-thin"></i></span></a>
                    <ul class="sub-menu">
                        <li class="back-sub-menu" data-depth="0"><i class='tg-icon-arrow-prev-thin'></i><span>Back</span></li>
                        <li id="menu-item-872" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-872"><a href="https://theme-one.com/the-grid/instagram-3/" class="menu-link-parent" data-depth="2">Instagram<span class="arrow-indicator"><i class="tg-icon-arrow-next-thin"></i></span></a>
                            <ul class="sub-menu">
                                <li class="back-sub-menu" data-depth="1"><i class='tg-icon-arrow-prev-thin'></i><span>Back</span></li>
                                <li id="menu-item-881" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-881"><a href="https://theme-one.com/the-grid/instagram/" class="menu-link-noparent">Example 1</a></li>
                                <li id="menu-item-880" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-880"><a href="https://theme-one.com/the-grid/instagram-2/" class="menu-link-noparent">Example 2</a></li>
                                <li id="menu-item-879" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-879"><a href="https://theme-one.com/the-grid/instagram-3/" class="menu-link-noparent">Example 3</a></li>
                            </ul>
                        </li>
                        <li id="menu-item-871" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-871"><a href="https://theme-one.com/the-grid/youtube-3/" class="menu-link-parent" data-depth="2">Youtube<span class="arrow-indicator"><i class="tg-icon-arrow-next-thin"></i></span></a>
                            <ul class="sub-menu">
                                <li class="back-sub-menu" data-depth="1"><i class='tg-icon-arrow-prev-thin'></i><span>Back</span></li>
                                <li id="menu-item-878" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-878"><a href="https://theme-one.com/the-grid/youtube/" class="menu-link-noparent">Example 1</a></li>
                                <li id="menu-item-877" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-877"><a href="https://theme-one.com/the-grid/youtube-2/" class="menu-link-noparent">Example 2</a></li>
                                <li id="menu-item-876" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-876"><a href="https://theme-one.com/the-grid/youtube-3/" class="menu-link-noparent">Example 3</a></li>
                            </ul>
                        </li>
                        <li id="menu-item-870" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-870"><a href="https://theme-one.com/the-grid/vimeo-2/" class="menu-link-parent" data-depth="2">Vimeo<span class="arrow-indicator"><i class="tg-icon-arrow-next-thin"></i></span></a>
                            <ul class="sub-menu">
                                <li class="back-sub-menu" data-depth="1"><i class='tg-icon-arrow-prev-thin'></i><span>Back</span></li>
                                <li id="menu-item-875" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-875"><a href="https://theme-one.com/the-grid/vimeo/" class="menu-link-noparent">Example 1</a></li>
                                <li id="menu-item-874" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-874"><a href="https://theme-one.com/the-grid/vimeo-2/" class="menu-link-noparent">Example 2</a></li>
                                <li id="menu-item-873" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-873"><a href="https://theme-one.com/the-grid/vimeo-3/" class="menu-link-noparent">Example 3</a></li>
                            </ul>
                        </li>
                        <li id="menu-item-1173" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1173"><a href="https://theme-one.com/the-grid/facebook/" class="menu-link-parent" data-depth="2">Facebook<span class="arrow-indicator"><i class="tg-icon-arrow-next-thin"></i></span></a>
                            <ul class="sub-menu">
                                <li class="back-sub-menu" data-depth="1"><i class='tg-icon-arrow-prev-thin'></i><span>Back</span></li>
                                <li id="menu-item-1172" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1172"><a href="https://theme-one.com/the-grid/facebook/" class="menu-link-noparent">Example 1</a></li>
                                <li id="menu-item-1171" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1171"><a href="https://theme-one.com/the-grid/facebook-2/" class="menu-link-noparent">Example 2</a></li>
                                <li id="menu-item-1170" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1170"><a href="https://theme-one.com/the-grid/facebook-3/" class="menu-link-noparent">Example 3</a></li>
                            </ul>
                        </li>
                        <li id="menu-item-1169" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1169"><a href="https://theme-one.com/the-grid/twitter-3/" class="menu-link-parent" data-depth="2">Twitter<span class="arrow-indicator"><i class="tg-icon-arrow-next-thin"></i></span></a>
                            <ul class="sub-menu">
                                <li class="back-sub-menu" data-depth="1"><i class='tg-icon-arrow-prev-thin'></i><span>Back</span></li>
                                <li id="menu-item-1168" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1168"><a href="https://theme-one.com/the-grid/twitter/" class="menu-link-noparent">Example 1</a></li>
                                <li id="menu-item-1167" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1167"><a href="https://theme-one.com/the-grid/twitter-2/" class="menu-link-noparent">Example 2</a></li>
                                <li id="menu-item-1166" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1166"><a href="https://theme-one.com/the-grid/twitter-3/" class="menu-link-noparent">Example 3</a></li>
                            </ul>
                        </li>
                        <li id="menu-item-1165" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1165"><a href="https://theme-one.com/the-grid/flickr-3/" class="menu-link-parent" data-depth="2">Flickr<span class="arrow-indicator"><i class="tg-icon-arrow-next-thin"></i></span></a>
                            <ul class="sub-menu">
                                <li class="back-sub-menu" data-depth="1"><i class='tg-icon-arrow-prev-thin'></i><span>Back</span></li>
                                <li id="menu-item-1164" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1164"><a href="https://theme-one.com/the-grid/flickr/" class="menu-link-noparent">Example 1</a></li>
                                <li id="menu-item-1163" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1163"><a href="https://theme-one.com/the-grid/flickr-2/" class="menu-link-noparent">Example 2</a></li>
                                <li id="menu-item-1162" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1162"><a href="https://theme-one.com/the-grid/flickr-3/" class="menu-link-noparent">Example 3</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li id="menu-item-255" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-255"><a href="https://theme-one.com/the-grid/features/" class="menu-link-noparent">Features</a></li>
                <li id="menu-item-709" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-709"><a href="https://theme-one.com/docs/the-grid/" class="menu-link-noparent">Documentation</a></li>
            </ul>
        </nav>
    </div>
</div>
<div id="body-overlay" style="background: #ee1a59;"></div>

<div id="outer-container" style="margin-bottom: 0px;background: #3CB878;">
    <div id="push-anim-overlay"></div>
    <div id="vc-spacer" style="height:100px;"></div>
    <div id="inner-container" class="dark">

        <section>
            <div class="tg-grid-wrapper slider-3d tg-grid-loading tg-nav-bg" id="grid-770" data-version="2.6.60">
                <style class="tg-grid-styles" type="text/css" scoped>
                    #grid-770 .tg-nav-color:not(.dots):not(.tg-dropdown-value):not(.tg-dropdown-title):hover,
                    #grid-770 .tg-nav-color:hover .tg-nav-color,
                    #grid-770 .tg-filter-active span,
                    #grid-770 .tg-page-number.tg-page-current {
                        color: #fff
                    }

                    #grid-770 .tg-filter:not(.tg-dropdown-item),
                    #grid-770 .tg-search-holder,
                    #grid-770 .tg-dropdown-holder,
                    #grid-770 .tg-sorter-order,
                    #grid-770 .tg-left-arrow,
                    #grid-770 .tg-right-arrow,
                    #grid-770 .tg-search-holder,
                    #grid-770 .tg-page-number:not(.dots),
                    #grid-770 .tg-pagination-prev,
                    #grid-770 .tg-pagination-next,
                    #grid-770 .tg-ajax-button {
                        background: #fff
                    }

                    #grid-770 .tg-filter:not(.tg-dropdown-item):hover,
                    #grid-770 .tg-filter.tg-filter-active,
                    #grid-770 .tg-sorter-order:hover,
                    #grid-770 .tg-left-arrow:not(.tg-disabled):hover,
                    #grid-770 .tg-right-arrow:not(.tg-disabled):hover,
                    #grid-770 .tg-page-number.tg-page-current,
                    #grid-770 .tg-page-number:not(.dots):hover,
                    #grid-770 .tg-pagination-prev:hover,
                    #grid-770 .tg-pagination-next:hover,
                    #grid-770 .tg-ajax-button:hover {
                        background: #2fbfc1
                    }

                    .tg-nav-bg input[type=text].tg-search {
                        height: 34px
                    }

                    .tg-nav-bg .tg-nav-font,
                    .tg-nav-bg input[type=text].tg-search {
                        font-size: 13px;
                        line-height: 34px
                    }

                    .tg-nav-bg .tg-search::-webkit-input-placeholder {
                        font-size: 13px;
                        line-height: 34px
                    }

                    .tg-nav-bg .tg-search::-moz-placeholder {
                        font-size: 13px;
                        line-height: 34px
                    }

                    .tg-nav-bg .tg-search:-ms-input-placeholder {
                        font-size: 13px;
                        line-height: 34px
                    }

                    .tg-nav-bg .tg-page-number.dots,
                    .tg-nav-bg .tg-slider-bullets {
                        height: 34px
                    }

                    .tg-nav-bg .tg-search-icon,
                    .tg-nav-bg .tg-search-clear,
                    .tg-nav-bg .tg-sorter-order,
                    .tg-nav-bg .tg-page-number,
                    .tg-nav-bg .tg-left-arrow i,
                    .tg-nav-bg .tg-right-arrow i {
                        min-width: 34px
                    }

                    .tg-nav-bg .tg-dropdown-item {
                        font-weight: 400
                    }

                    .tg-nav-bg .tg-dropdown-item {
                        text-transform: none
                    }

                    #grid-770 .tg-nav-color,
                    #grid-770 .tg-search-icon:hover:before,
                    #grid-770 .tg-search-icon:hover input,
                    #grid-770 .tg-disabled:hover .tg-icon-left-arrow,
                    #grid-770 .tg-disabled:hover .tg-icon-right-arrow,
                    #grid-770 .tg-dropdown-title.tg-nav-color:hover {
                        color: #777
                    }

                    #grid-770 input.tg-search:hover {
                        color: #777777!important
                    }

                    #grid-770 input.tg-search::-webkit-input-placeholder {
                        color: #777
                    }

                    #grid-770 input.tg-search::-moz-placeholder {
                        color: #777;
                        opacity: 1
                    }

                    #grid-770 input.tg-search:-ms-input-placeholder {
                        color: #777
                    }

                    .grid-770 .tg-dropdown-item {
                        color: #777;
                        background: #fff
                    }

                    .grid-770 .tg-filter-active,
                    .grid-770 .tg-dropdown-item:hover {
                        color: #fff;
                        background: #2fbfc1
                    }

                    #grid-770 .tg-left-arrow i,
                    #grid-770 .tg-right-arrow i {
                        background: rgba(0, 0, 0, .4);
                        color: #fff
                    }

                    #grid-770 .tg-slider-bullets li.tg-active-item span {
                        background: #2fbfc1
                    }

                    #grid-770 .tg-slider-bullets li span {
                        background: #DDD
                    }

                    #grid-770 .tg-grid-area-top2 {
                        text-align: center;
                        margin-top: 60px;
                        margin-bottom: 60px;
                        margin-left: -25%;
                    }

                    #grid-770 .tg-grid-area-left {
                        margin-left: 10px
                    }

                    #grid-770 .tg-grid-area-right {
                        margin-right: 10px
                    }

                    #grid-770 .tg-grid-area-bottom1 {
                        text-align: center;
                        margin-top: 30px
                    }

                    .alofi a,
                    .alofi a:active,
                    .alofi a:focus,
                    .alofi .tg-item-overlay {
                        text-decoration: none;
                        border: none;
                        -webkit-box-shadow: none;
                        box-shadow: none;
                        -webkit-transition: opacity 0.25s ease, color 0.25s ease, -webkit-transform 0.3s ease-in-out;
                        -moz-transition: opacity 0.25s ease, color 0.25s ease, -moz-transform 0.3s ease-in-out;
                        -ms-transition: opacity 0.25s ease, color 0.25s ease, -ms-transform 0.3s ease-in-out;
                        -o-transition: opacity 0.25s ease, color 0.25s ease, -o-transform 0.3s ease-in-out;
                        transition: opacity 0.25s ease, color 0.25s ease, transform 0.3s ease-in-out
                    }

                    .alofi .tg-item-link {
                        position: absolute;
                        display: block;
                        top: 0;
                        left: 0;
                        right: 0;
                        bottom: 0
                    }

                    .tg-item .tg-dark div,
                    .tg-item .tg-dark h1,
                    .tg-item .tg-dark h1 a,
                    .tg-item .tg-dark h2,
                    .tg-item .tg-dark h2 a,
                    .tg-item .tg-dark h3,
                    .tg-item .tg-dark h3 a,
                    .tg-item .tg-dark h4,
                    .tg-item .tg-dark h4 a,
                    .tg-item .tg-dark h5,
                    .tg-item .tg-dark h5 a,
                    .tg-item .tg-dark h6,
                    .tg-item .tg-dark h6 a,
                    .tg-item .tg-dark a,
                    .tg-item .tg-dark a.tg-link-url,
                    .tg-item .tg-dark i,
                    .tg-item .tg-dark .tg-media-button,
                    .tg-item .tg-dark .tg-item-price span {
                        color: #444;
                        fill: #444;
                        stroke: #444;
                        border-color: #444
                    }

                    .tg-item .tg-dark p,
                    .tg-item .tg-dark ol,
                    .tg-item .tg-dark ul,
                    .tg-item .tg-dark li {
                        color: #777;
                        fill: #777;
                        stroke: #777;
                        border-color: #777
                    }

                    .tg-item .tg-dark span,
                    .tg-item .tg-dark .no-liked .to-heart-icon path,
                    .tg-item .tg-dark .empty-heart .to-heart-icon path,
                    .tg-item .tg-dark .tg-item-comment i,
                    .tg-item .tg-dark .tg-item-price del span {
                        color: #999;
                        fill: #999;
                        stroke: #999;
                        border-color: #999
                    }

                    .tg-item .tg-light div,
                    .tg-item .tg-light h1,
                    .tg-item .tg-light h1 a,
                    .tg-item .tg-light h2,
                    .tg-item .tg-light h2 a,
                    .tg-item .tg-light h3,
                    .tg-item .tg-light h3 a,
                    .tg-item .tg-light h4,
                    .tg-item .tg-light h4 a,
                    .tg-item .tg-light h5,
                    .tg-item .tg-light h5 a,
                    .tg-item .tg-light h6,
                    .tg-item .tg-light h6 a,
                    .tg-item .tg-light a,
                    .tg-item .tg-light a.tg-link-url,
                    .tg-item .tg-light i,
                    .tg-item .tg-light .tg-media-button,
                    .tg-item .tg-light .tg-item-price span {
                        color: #fff;
                        fill: #fff;
                        stroke: #fff;
                        border-color: #fff
                    }

                    .tg-item .tg-light p,
                    .tg-item .tg-light ol,
                    .tg-item .tg-light ul,
                    .tg-item .tg-light li {
                        color: #f6f6f6;
                        fill: #f6f6f6;
                        stroke: #f6f6f6;
                        border-color: #f6f6f6
                    }

                    .tg-item .tg-light span,
                    .tg-item .tg-light .no-liked .to-heart-icon path,
                    .tg-item .tg-light .empty-heart .to-heart-icon path,
                    .tg-item .tg-light .tg-item-comment i,
                    .tg-item .tg-light .tg-item-price del span {
                        color: #f5f5f5;
                        fill: #f5f5f5;
                        stroke: #f5f5f5;
                        border-color: #f5f5f5
                    }

                    #grid-770 .tg-item-content-holder {
                        background-color: #fff
                    }

                    #grid-770 .tg-item-overlay {
                        background-color: rgba(47, 191, 193, .65)
                    }
                </style>
                <div class="tg-grid-sizer"></div>
                <div class="tg-gutter-sizer"></div>
                <div class="tg-grid-area-top2">
                    <div class="tg-filters-holder">
                        <div class="tg-filter tg-filter-active tg-nav-color tg-nav-border tg-nav-font" data-filter="*"><span class="tg-filter-name tg-nav-color tg-nav-font">All</span></div>
                        <div class="tg-filter tg-nav-color tg-nav-border tg-nav-font" data-taxo="media-category" data-filter=".f42"><span class="tg-filter-name tg-nav-color tg-nav-font">Animals</span></div>
                        <div class="tg-filter tg-nav-color tg-nav-border tg-nav-font" data-taxo="media-category" data-filter=".f43"><span class="tg-filter-name tg-nav-color tg-nav-font">Clouds</span></div>
                        <div class="tg-filter tg-nav-color tg-nav-border tg-nav-font" data-taxo="media-category" data-filter=".f41"><span class="tg-filter-name tg-nav-color tg-nav-font">Mountains</span></div>
                        <div class="tg-filter tg-nav-color tg-nav-border tg-nav-font" data-taxo="media-category" data-filter=".f40"><span class="tg-filter-name tg-nav-color tg-nav-font">Nature</span></div>
                        <div class="tg-filter tg-nav-color tg-nav-border tg-nav-font" data-taxo="media-category" data-filter=".f45"><span class="tg-filter-name tg-nav-color tg-nav-font">People</span></div>
                        <div class="tg-filter tg-nav-color tg-nav-border tg-nav-font" data-taxo="media-category" data-filter=".f44"><span class="tg-filter-name tg-nav-color tg-nav-font">Vegetals</span></div>
                    </div>
                </div>
                <div class="tg-grid-slider">
                    <div class="tg-grid-holder tg-layout-grid" data-name="Gallery Slider" data-style="grid" data-row="2" data-layout="horizontal" data-rtl="" data-fitrows="" data-filtercomb="" data-filterlogic="AND" data-filterload="" data-sortbyload="none" data-orderload="false" data-fullwidth="" data-fullheight="" data-gutters="[[320,0],[480,0],[768,0],[980,0],[1200,0],[9999,0]]" data-slider='{"itemNav":"basic","swingSpeed":0.1,"cycleBy":"null","cycle":5000,"startAt":1}' data-ratio="1.33" data-cols="[[320,1],[480,1],[768,2],[980,3],[1200,4],[9999,5]]" data-rows="[[320,200],[480,200],[768,220],[980,220],[1200,240],[9999,240]]" data-animation='{&quot;name&quot;:&quot;From Right Flip Z&quot;,&quot;visible&quot;:&quot;perspective(2000px) translateY(0) rotate3d(0,0,1,0deg) scale(1)&quot;,&quot;hidden&quot;:&quot;perspective(2000px) translateX(100px) rotate3d(0,0,1,45deg) scale(0.2)&quot;}' data-transition="700ms" data-ajaxmethod="" data-ajaxdelay="100" data-preloader="1" data-itemdelay="100" data-gallery="" data-ajax="">
                        <article class="tg-item tg-post-738 alofi f45 tg-item-reveal" data-id="738" data-date="1445861965" data-title="Umbrella" data-row="1" data-col="1">
                            <div class="tg-item-inner">
                                <div class="tg-item-media-holder tg-light">
                                    <div class="tg-item-media-inner">
                                        <div class="tg-item-image" style="background-image: url(https://theme-one.com/the-grid/wp-content/uploads/sites/6/2015/10/umbrella.jpg)"></div>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <article class="tg-item tg-post-746 alofi f43 f41 f45 tg-item-reveal" data-id="746" data-date="1445861979" data-title="Mountains" data-row="1" data-col="1">
                            <div class="tg-item-inner">
                                <div class="tg-item-media-holder tg-light">
                                    <div class="tg-item-media-inner">
                                        <div class="tg-item-image" style="background-image: url(https://theme-one.com/the-grid/wp-content/uploads/sites/6/2015/10/mountain.jpg)"></div>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <article class="tg-item tg-post-748 alofi f40 f45 tg-item-reveal" data-id="748" data-date="1445861982" data-title="Photograph" data-row="1" data-col="1">
                            <div class="tg-item-inner">
                                <div class="tg-item-media-holder tg-light">
                                    <div class="tg-item-media-inner">
                                        <div class="tg-item-image" style="background-image: url(https://theme-one.com/the-grid/wp-content/uploads/sites/6/2015/10/photograph.jpg)"></div>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <article class="tg-item tg-post-742 alofi f41 f45 tg-item-reveal" data-id="742" data-date="1445861971" data-title="Climb the mo" data-row="2" data-col="2">
                            <div class="tg-item-inner">
                                <div class="tg-item-media-holder tg-light">
                                    <div class="tg-item-media-inner">
                                        <div class="tg-item-image" style="background-image: url(https://theme-one.com/the-grid/wp-content/uploads/sites/6/2015/10/climb-mountain.jpg)"></div>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <article class="tg-item tg-post-736 alofi f40 tg-item-reveal" data-id="736" data-date="1445861961" data-title="Staircase" data-row="2" data-col="1">
                            <div class="tg-item-inner">
                                <div class="tg-item-media-holder tg-light">
                                    <div class="tg-item-media-inner">
                                        <div class="tg-item-image" style="background-image: url(https://theme-one.com/the-grid/wp-content/uploads/sites/6/2015/10/staircase.jpg)"></div>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <article class="tg-item tg-post-739 alofi f44 tg-item-reveal" data-id="739" data-date="1445861966" data-title="Vegetals" data-row="1" data-col="1">
                            <div class="tg-item-inner">
                                <div class="tg-item-media-holder tg-light">
                                    <div class="tg-item-media-inner">
                                        <div class="tg-item-image" style="background-image: url(https://theme-one.com/the-grid/wp-content/uploads/sites/6/2015/10/vegetals.jpg)"></div>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <article class="tg-item tg-post-740 alofi f42 f40 tg-item-reveal" data-id="740" data-date="1445861968" data-title="Wolf" data-row="1" data-col="1">
                            <div class="tg-item-inner">
                                <div class="tg-item-media-holder tg-light">
                                    <div class="tg-item-media-inner">
                                        <div class="tg-item-image" style="background-image: url(https://theme-one.com/the-grid/wp-content/uploads/sites/6/2015/10/wolf.jpg)"></div>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <article class="tg-item tg-post-745 alofi f42 f40 tg-item-reveal" data-id="745" data-date="1445861977" data-title="Fox" data-row="1" data-col="1">
                            <div class="tg-item-inner">
                                <div class="tg-item-media-holder tg-light">
                                    <div class="tg-item-media-inner">
                                        <div class="tg-item-image" style="background-image: url(https://theme-one.com/the-grid/wp-content/uploads/sites/6/2015/10/fox.jpg)"></div>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <article class="tg-item tg-post-744 alofi f45 f44 tg-item-reveal" data-id="744" data-date="1445861975" data-title="Forest" data-row="1" data-col="1">
                            <div class="tg-item-inner">
                                <div class="tg-item-media-holder tg-light">
                                    <div class="tg-item-media-inner">
                                        <div class="tg-item-image" style="background-image: url(https://theme-one.com/the-grid/wp-content/uploads/sites/6/2015/10/forest.jpg)"></div>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <article class="tg-item tg-post-747 alofi f43 tg-item-reveal" data-id="747" data-date="1445861981" data-title="Mountain and" data-row="1" data-col="2">
                            <div class="tg-item-inner">
                                <div class="tg-item-media-holder tg-light">
                                    <div class="tg-item-media-inner">
                                        <div class="tg-item-image" style="background-image: url(https://theme-one.com/the-grid/wp-content/uploads/sites/6/2015/10/mountain-clouds.jpg)"></div>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <article class="tg-item tg-post-735 alofi f45 tg-item-reveal" data-id="735" data-date="1445861959" data-title="Skateboardin" data-row="1" data-col="2">
                            <div class="tg-item-inner">
                                <div class="tg-item-media-holder tg-light">
                                    <div class="tg-item-media-inner">
                                        <div class="tg-item-image" style="background-image: url(https://theme-one.com/the-grid/wp-content/uploads/sites/6/2015/10/skateboarding.jpg)"></div>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <article class="tg-item tg-post-743 alofi f43 tg-item-reveal" data-id="743" data-date="1445861973" data-title="Clouds" data-row="1" data-col="1">
                            <div class="tg-item-inner">
                                <div class="tg-item-media-holder tg-light">
                                    <div class="tg-item-media-inner">
                                        <div class="tg-item-image" style="background-image: url(https://theme-one.com/the-grid/wp-content/uploads/sites/6/2015/10/clouds.jpg)"></div>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <article class="tg-item tg-post-737 alofi f45 tg-item-reveal" data-id="737" data-date="1445861963" data-title="Swings" data-row="1" data-col="1">
                            <div class="tg-item-inner">
                                <div class="tg-item-media-holder tg-light">
                                    <div class="tg-item-media-inner">
                                        <div class="tg-item-image" style="background-image: url(https://theme-one.com/the-grid/wp-content/uploads/sites/6/2015/10/swings.jpg)"></div>
                                    </div>
                                </div>
                            </div>
                        </article>
                        <article class="tg-item tg-post-741 alofi f40 f44 tg-item-reveal" data-id="741" data-date="1445861970" data-title="Bamboo" data-row="1" data-col="1">
                            <div class="tg-item-inner">
                                <div class="tg-item-media-holder tg-light">
                                    <div class="tg-item-media-inner">
                                        <div class="tg-item-image" style="background-image: url(https://theme-one.com/the-grid/wp-content/uploads/sites/6/2015/10/bamboo.jpg)"></div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="tg-grid-area-left">
                        <div class="tg-grid-area-inner">
                            <div class="tg-grid-area-wrapper">
                                <div class="tg-left-arrow tg-nav-color tg-nav-font"><i class="tg-icon-left-arrow tg-nav-color tg-nav-border tg-nav-font"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="tg-grid-area-right">
                        <div class="tg-grid-area-inner">
                            <div class="tg-grid-area-wrapper">
                                <div class="tg-right-arrow tg-nav-color tg-nav-font"><i class="tg-icon-right-arrow tg-nav-color tg-nav-border tg-nav-font"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tg-grid-area-bottom1">
                    <div class="tg-slider-bullets-holder">
                        <div class="tg-slider-bullets"></div>
                    </div>
                </div>
                <div class="tg-grid-preloader">
                    <style class="tg-grid-preloader-styles" type="text/css" scoped>
                        .pacman>div:first-of-type,
                        .pacman>div:nth-child(2) {
                            width: 0;
                            height: 0;
                            border-right: 25px solid transparent!important;
                            border-top: 25px solid;
                            border-left: 25px solid;
                            border-bottom: 25px solid;
                            border-radius: 25px;
                            position: relative;
                            left: -30px;
                            background-color: transparent!important
                        }

                        @-webkit-keyframes rotate_pacman_half_up {
                            0%,
                            100% {
                                -webkit-transform: rotate(270deg);
                                transform: rotate(270deg)
                            }
                            50% {
                                -webkit-transform: rotate(360deg);
                                transform: rotate(360deg)
                            }
                        }

                        @keyframes rotate_pacman_half_up {
                            0%,
                            100% {
                                -webkit-transform: rotate(270deg);
                                transform: rotate(270deg)
                            }
                            50% {
                                -webkit-transform: rotate(360deg);
                                transform: rotate(360deg)
                            }
                        }

                        @-webkit-keyframes rotate_pacman_half_down {
                            0%,
                            100% {
                                -webkit-transform: rotate(90deg);
                                transform: rotate(90deg)
                            }
                            50% {
                                -webkit-transform: rotate(0);
                                transform: rotate(0)
                            }
                        }

                        @keyframes rotate_pacman_half_down {
                            0%,
                            100% {
                                -webkit-transform: rotate(90deg);
                                transform: rotate(90deg)
                            }
                            50% {
                                -webkit-transform: rotate(0);
                                transform: rotate(0)
                            }
                        }

                        @-webkit-keyframes pacman-balls {
                            0% {
                                opacity: 0;
                                -webkit-transform: translate3d(0, 0, 0);
                                transform: translate3d(0, 0, 0)
                            }
                            75% {
                                opacity: .7;
                                -webkit-transform: translate3d(-75px, 0, 0);
                                transform: translate3d(-75px, 0, 0)
                            }
                            100% {
                                opacity: 1;
                                -webkit-transform: translate3d(-100px, 0, 0);
                                transform: translate3d(-100px, 0, 0)
                            }
                        }

                        @keyframes pacman-balls {
                            0% {
                                opacity: 0;
                                -webkit-transform: translate3d(0, 0, 0);
                                transform: translate3d(0, 0, 0)
                            }
                            75% {
                                opacity: .7;
                                -webkit-transform: translate3d(-75px, 0, 0);
                                transform: translate3d(-75px, 0, 0)
                            }
                            100% {
                                opacity: 1;
                                -webkit-transform: translate3d(-100px, 0, 0);
                                transform: translate3d(-100px, 0, 0)
                            }
                        }

                        .pacman {
                            position: relative
                        }

                        .pacman>div:nth-child(3) {
                            -webkit-animation: pacman-balls 1s -.66s infinite linear;
                            animation: pacman-balls 1s -.66s infinite linear
                        }

                        .pacman>div:nth-child(4) {
                            -webkit-animation: pacman-balls 1s -.33s infinite linear;
                            animation: pacman-balls 1s -.33s infinite linear
                        }

                        .pacman>div:nth-child(5) {
                            -webkit-animation: pacman-balls 1s -.01s infinite linear;
                            animation: pacman-balls 1s -.01s infinite linear
                        }

                        .pacman>div:first-of-type {
                            -webkit-animation: rotate_pacman_half_up .45s 0s infinite;
                            animation: rotate_pacman_half_up .45s 0s infinite
                        }

                        .pacman>div:nth-child(2) {
                            -webkit-animation: rotate_pacman_half_down .45s 0s infinite;
                            animation: rotate_pacman_half_down .45s 0s infinite;
                            margin-top: -50px
                        }

                        .pacman>div:nth-child(3),
                        .pacman>div:nth-child(4),
                        .pacman>div:nth-child(5),
                        .pacman>div:nth-child(6) {
                            position: absolute;
                            display: block;
                            border-radius: 100%;
                            width: 10px;
                            height: 10px;
                            top: 50%;
                            margin-top: -5px;
                            left: 80px
                        }

                        #grid-770 .tg-grid-preloader-inner>div {
                            background: #2fbfc1
                        }

                        #grid-770 .tg-grid-preloader-inner>div {
                            border-color: #2fbfc1
                        }

                        #grid-770 .tg-grid-preloader-scale {
                            transform: scale(1)
                        }
                    </style>
                    <div class="tg-grid-preloader-holder">
                        <div class="tg-grid-preloader-scale">
                            <div class="tg-grid-preloader-inner pacman">
                                <div></div>
                                <div></div>
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
</div>

<script type="text/javascript">
    var c = document.body.className;
    c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
    document.body.className = c;
</script>
<script type='text/javascript' src='https://theme-one.com/the-grid/wp-content/plugins/yith-woocommerce-wishlist/assets/js/jquery.selectBox.min.js'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var yith_wcwl_l10n = {
        "ajax_url": "\/the-grid\/wp-admin\/admin-ajax.php",
        "redirect_to_cart": "no",
        "multi_wishlist": "",
        "hide_add_button": "1",
        "is_user_logged_in": "",
        "ajax_loader_url": "https:\/\/theme-one.com\/the-grid\/wp-content\/plugins\/yith-woocommerce-wishlist\/assets\/images\/ajax-loader.gif",
        "remove_from_wishlist_after_add_to_cart": "yes",
        "labels": {
            "cookie_disabled": "We are sorry, but this feature is available only if cookies are enabled on your browser.",
            "added_to_cart_message": "<div class=\"woocommerce-message\">Product correctly added to cart<\/div>"
        },
        "actions": {
            "add_to_wishlist_action": "add_to_wishlist",
            "remove_from_wishlist_action": "remove_from_wishlist",
            "move_to_another_wishlist_action": "move_to_another_wishlsit",
            "reload_wishlist_and_adding_elem_action": "reload_wishlist_and_adding_elem"
        }
    }; /* ]]> */
</script>
<script type='text/javascript' src='https://theme-one.com/the-grid/wp-content/plugins/yith-woocommerce-wishlist/assets/js/jquery.yith-wcwl.js'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var to_ajaxurl = "https:\/\/theme-one.com\/the-grid\/wp-admin\/admin-ajax.php"; /* ]]> */
</script>
<script type='text/javascript' src='https://theme-one.com/the-grid/wp-content/themes/grid-theme/js/custom.js'></script>
<script type='text/javascript' src='https://theme-one.com/the-grid/wp-includes/js/jquery/ui/effect.min.js'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var tg_global_var = {
        "url": "https:\/\/theme-one.com\/the-grid\/wp-admin\/admin-ajax.php",
        "nonce": "37b0b61407",
        "is_mobile": null,
        "mediaelement": "1",
        "mediaelement_ex": "1",
        "lightbox_autoplay": "",
        "debounce": "1",
        "meta_data": null,
        "main_query": {
            "page": 0,
            "pagename": "multi-rows-slider",
            "error": "",
            "m": "",
            "p": 0,
            "post_parent": "",
            "subpost": "",
            "subpost_id": "",
            "attachment": "",
            "attachment_id": 0,
            "name": "multi-rows-slider",
            "static": "",
            "page_id": 0,
            "second": "",
            "minute": "",
            "hour": "",
            "day": 0,
            "monthnum": 0,
            "year": 0,
            "w": 0,
            "category_name": "",
            "tag": "",
            "cat": "",
            "tag_id": "",
            "author": "",
            "author_name": "",
            "feed": "",
            "tb": "",
            "paged": 0,
            "meta_key": "",
            "meta_value": "",
            "preview": "",
            "s": "",
            "sentence": "",
            "title": "",
            "fields": "",
            "menu_order": "",
            "embed": "",
            "category__in": [],
            "category__not_in": [],
            "category__and": [],
            "post__in": [],
            "post__not_in": [],
            "post_name__in": [],
            "tag__in": [],
            "tag__not_in": [],
            "tag__and": [],
            "tag_slug__in": [],
            "tag_slug__and": [],
            "post_parent__in": [],
            "post_parent__not_in": [],
            "author__in": [],
            "author__not_in": [],
            "ignore_sticky_posts": false,
            "suppress_filters": false,
            "cache_results": true,
            "update_post_term_cache": true,
            "lazy_load_term_meta": true,
            "update_post_meta_cache": true,
            "post_type": "",
            "posts_per_page": 9,
            "nopaging": false,
            "comments_per_page": "50",
            "no_found_rows": false,
            "order": "DESC"
        }
    }; /* ]]> */
</script>
<script type='text/javascript' src='https://theme-one.com/the-grid/wp-content/plugins/the-grid/frontend/assets/js/the-grid.min.js'></script>
<script type='text/javascript' src='https://theme-one.com/the-grid/wp-includes/js/wp-embed.min.js'></script>
<script type="text/javascript">
    var to_like_post = {
        "url": "https://theme-one.com/the-grid/wp-admin/admin-ajax.php",
        "nonce": "1ab731c094"
    };
    ! function(t) {
        "use strict";
        t(document).ready(function() {
            t(document).on("click", ".to-post-like:not('.to-post-like-unactive')", function(e) {
                e.preventDefault();
                var o = t(this),
                    n = o.data("post-id"),
                    s = parseInt(o.find(".to-like-count").text());
                return o.addClass("heart-pulse"), t.ajax({
                    type: "post",
                    url: to_like_post.url,
                    data: {
                        nonce: to_like_post.nonce,
                        action: "to_like_post",
                        post_id: n,
                        like_nb: s
                    },
                    context: o,
                    success: function(e) {
                        e && ((o = t(this)).attr("title", e.title), o.find(".to-like-count").text(e.count), o.removeClass(e.remove_class + " heart-pulse").addClass(e.add_class))
                    }
                }), !1
            })
        })
    }(jQuery);
</script>
</body>

</html>
<!-- This website is like a Rocket, isn't it? Performance optimized by WP Rocket. Learn more: https://wp-rocket.me - Debug: cached@1539601681 -->