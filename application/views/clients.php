
<html>
<head>
    <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    <style>
        /* Slider */
        .slick-slide {
            margin: 0px 20px;
        }

        .slick-slide img {
            width: 100%;
        }

        .slick-slider
        {
            position: relative;

            display: block;
            box-sizing: border-box;

            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;

            -webkit-touch-callout: none;
            -khtml-user-select: none;
            -ms-touch-action: pan-y;
            touch-action: pan-y;
            -webkit-tap-highlight-color: transparent;
        }

        .slick-list
        {
            position: relative;

            display: block;
            overflow: hidden;

            margin: 0;
            padding: 0;
        }
        .slick-list:focus
        {
            outline: none;
        }
        .slick-list.dragging
        {
            cursor: pointer;
            cursor: hand;
        }

        .slick-slider .slick-track,
        .slick-slider .slick-list
        {
            -webkit-transform: translate3d(0, 0, 0);
            -moz-transform: translate3d(0, 0, 0);
            -ms-transform: translate3d(0, 0, 0);
            -o-transform: translate3d(0, 0, 0);
            transform: translate3d(0, 0, 0);
        }

        .slick-track
        {
            position: relative;
            top: 0;
            left: 0;

            display: block;
        }
        .slick-track:before,
        .slick-track:after
        {
            display: table;

            content: '';
        }
        .slick-track:after
        {
            clear: both;
        }
        .slick-loading .slick-track
        {
            visibility: hidden;
        }

        .slick-slide
        {
            display: none;
            float: left;

            height: 100%;
            min-height: 1px;
        }
        [dir='rtl'] .slick-slide
        {
            float: right;
        }
        .slick-slide img
        {
            display: block;
        }
        .slick-slide.slick-loading img
        {
            display: none;
        }
        .slick-slide.dragging img
        {
            pointer-events: none;
        }
        .slick-initialized .slick-slide
        {
            display: block;
        }
        .slick-loading .slick-slide
        {
            visibility: hidden;
        }
        .slick-vertical .slick-slide
        {
            display: block;

            height: auto;

            border: 1px solid transparent;
        }
        .slick-arrow.slick-hidden {
            display: none;
        }
    </style>
</head>
<body style="background-color: rgb(27, 206, 230);">

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"><div class="customer-logos">
    <div class="slide"><img src="<?php echo base_url();?>/images/clients/arun-exllo.png"></div>
    <div class="slide"><img src="<?php echo base_url();?>/images/clients/aloft_logo.jpg"></div>
    <div class="slide"><img src="<?php echo base_url();?>/images/clients/arcadio.png"></div>
    <div class="slide"><img src="<?php echo base_url();?>/images/clients/AYUSH.png"></div>
    <div class="slide"><img src="<?php echo base_url();?>/images/clients/CDS.png"></div>
    <div class="slide"><img src="<?php echo base_url();?>/images/clients/danfoss.png"></div>
    <div class="slide"><img src="<?php echo base_url();?>/images/clients/DSART.png"></div>
    <div class="slide"><img src="<?php echo base_url();?>/images/clients/GMT_logo.jpg"></div>
    <div class="slide"><img src="<?php echo base_url();?>/images/clients/HNS.png"></div>
    <div class="slide"><img src="<?php echo base_url();?>/images/clients/indiabulls.png"></div>
    <div class="slide"><img src="<?php echo base_url();?>/images/clients/LADY.png"></div>
    <div class="slide"><img src="<?php echo base_url();?>/images/clients/madras-khaki-logo.jpg"></div>
    <div class="slide"><img src="<?php echo base_url();?>/images/clients/madrassteels.png"></div>
    <div class="slide"><img src="<?php echo base_url();?>/images/clients/MC.png"></div>
    <div class="slide"><img src="<?php echo base_url();?>/images/clients/odyssey-logo.jpg"></div>
    <div class="slide"><img src="<?php echo base_url();?>/images/clients/PANCH.png"></div>
    <div class="slide"><img src="<?php echo base_url();?>/images/clients/PASTRY.png"></div>
    <div class="slide"><img src="<?php echo base_url();?>/images/clients/RAAHAT.png"></div>
    <div class="slide"><img src="<?php echo base_url();?>/images/clients/radiance_automation.jpg"></div>
    <div class="slide"><img src="<?php echo base_url();?>/images/clients/rudraa.png"></div>
    <div class="slide"><img src="<?php echo base_url();?>/images/clients/shyaway-logo.jpg"></div>
    <div class="slide"><img src="<?php echo base_url();?>/images/clients/sigma_lift.jpg"></div>
    <div class="slide"><img src="<?php echo base_url();?>/images/clients/sugalanddamani_color.png"></div>
    <div class="slide"><img src="<?php echo base_url();?>/images/clients/times-property.jpg"></div>
    <div class="slide"><img src="<?php echo base_url();?>/images/clients/ULTRAA.png"></div>
    <div class="slide"><img src="<?php echo base_url();?>/images/clients/volvo.jpg"></div>

</div>
</body>
<script>
    $(document).ready(function(){
        $('.customer-logos').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1000,
            arrows: false,
            dots: false,
            pauseOnHover: false,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 3
                }
            }, {
                breakpoint: 520,
                settings: {
                    slidesToShow: 2
                }
            }]
        });
    });
</script>
</html>
